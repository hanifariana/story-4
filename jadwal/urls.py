from django.urls import path
from .views import join, delete, detail

app_name = 'jadwal'

urlpatterns = [
    path('', join, name='Schedule'),
    path('<int:pk>', delete, name = 'delete'),
    path('detail/<int:pk>', detail, name='detail'),
    
    # path('profile/', views.profile, name='profile'),
    # dilanjutkan ...
]