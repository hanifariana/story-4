from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('about_me/', views.about_me, name='about_me'),
    path('mmrs/', views.mmrs, name='mmrs'),
    path('socmed/', views.socmed, name='socmed'),

    # dilanjutkan ...
]