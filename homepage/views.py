from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home.html')

def about_me(request):
    return render(request, 'about_me.html')

def mmrs(request):
    return render(request, 'mmrs.html')

def socmed(request):
    return render(request, 'socmed.html')